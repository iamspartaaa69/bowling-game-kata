﻿using NUnit.Framework;
using System;
using BowlingGameAttempt2;
namespace BowlingGame2Tests
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void CreateGame()
        {
            game = new Game();
        }

        void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
                game.Roll(pins);
        }

        [Test]
        public void OnePinFalls()
        {
            RollMany(20, 1);

            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test]
        public void GutterGame() 
        {
            RollMany(20, 0);

            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void OneSpare()
        {
            game.Roll(9); game.Roll(1);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(29));
        }
    }
}
