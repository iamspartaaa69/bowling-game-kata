﻿using System;

namespace BowlingGameAttempt2                             

{
    public class Game
    {
        int[] pinfalls = new int[21];
        int currentRoll = 0;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void Roll (int pins)
        {
            pinfalls[currentRoll] = pins;
            currentRoll++;
        }

        public int Score()
        {
            int score = 0;
            for (int i = 0; i < pinfalls.Length; i++)
                score += pinfalls[i];

            return score;
        }
    }
}
